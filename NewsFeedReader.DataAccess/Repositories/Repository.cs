﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NewsFeedReader.Domain;
using NewsFeedReader.Domain.Repositories;

namespace NewsFeedReader.DataAccess.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly DbContext context;
        private readonly IDbSet<TEntity> dbEntitySet;
        private bool disposed;

        public Repository(DbContext context)
        {
            this.context = context;
            dbEntitySet = this.context.Set<TEntity>();
        }

        public List<TEntity> GetAll()
        {
            return dbEntitySet.ToList();
        }

        public List<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.ToList();
        }

        public TEntity GetSingle(int id)
        {
            return dbEntitySet.FirstOrDefault(t => t.Id == id);
        }

        public TEntity GetSingleIncluding(int id, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.FirstOrDefault(x => x.Id == id);
        }

        public List<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return dbEntitySet.Where(predicate).ToList();
        }

        public void Insert(TEntity entity)
        {
            UpdateEntityState(entity, EntityState.Added);
            context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            UpdateEntityState(entity, EntityState.Modified);
            context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            UpdateEntityState(entity, EntityState.Deleted);
            context.SaveChanges();
        }

        public Task<List<TEntity>> GetAllAsync()
        {
            return dbEntitySet.ToListAsync();
        }

        public Task<List<TEntity>> GetAllIncludingAsync(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.ToListAsync();
        }

        public Task<TEntity> GetSingleAsync(int id)
        {
            return dbEntitySet.FirstOrDefaultAsync(t => t.Id == id);
        }

        public Task<TEntity> GetSingleIncludingAsync(int id, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<List<TEntity>> FindByAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return dbEntitySet.Where(predicate).ToListAsync();
        }
        
        private IQueryable<TEntity> FilterQuery(Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (predicate != null) ? entities.Where(predicate) : entities;
            return entities;
        }

        private IQueryable<TEntity> IncludeProperties(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> entities = dbEntitySet;
            foreach (var includeProperty in includeProperties)
            {
                entities = entities.Include(includeProperty);
            }
            return entities;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                context.Dispose();
            }
            disposed = true;
        }

        private void UpdateEntityState<TEntity>(TEntity entity, EntityState entityState) where TEntity : BaseEntity
        {
            var dbEntityEntry = GetDbEntityEntrySafely(entity);
            dbEntityEntry.State = entityState;
        }

        private DbEntityEntry GetDbEntityEntrySafely<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            var dbEntityEntry = context.Entry<TEntity>(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                context.Set<TEntity>().Attach(entity);
            }
            return dbEntityEntry;
        }
    }
}
