﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsFeedReader.Domain;
using NewsFeedReader.Domain.Repositories;

namespace NewsFeedReader.DataAccess.Repositories
{
    public class FeedRepository : Repository<Feed>, IFeedRepository
    {
        public FeedRepository(DbContext context) : base(context)
        {
        }
    }
}