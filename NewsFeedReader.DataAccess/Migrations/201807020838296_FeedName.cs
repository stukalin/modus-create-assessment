namespace NewsFeedReader.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FeedName : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Feeds", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Feeds", new[] { "ApplicationUserId" });
            AddColumn("dbo.Feeds", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Feeds", "Url", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Feeds", "ApplicationUserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Feeds", "ApplicationUserId");
            AddForeignKey("dbo.Feeds", "ApplicationUserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Feeds", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Feeds", new[] { "ApplicationUserId" });
            AlterColumn("dbo.Feeds", "ApplicationUserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Feeds", "Url", c => c.String());
            DropColumn("dbo.Feeds", "Name");
            CreateIndex("dbo.Feeds", "ApplicationUserId");
            AddForeignKey("dbo.Feeds", "ApplicationUserId", "dbo.AspNetUsers", "Id");
        }
    }
}
