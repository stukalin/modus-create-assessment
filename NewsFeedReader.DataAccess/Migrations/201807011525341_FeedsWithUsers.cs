namespace NewsFeedReader.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FeedsWithUsers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Feeds", "ApplicationUserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Feeds", "ApplicationUserId");
            AddForeignKey("dbo.Feeds", "ApplicationUserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Feeds", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Feeds", new[] { "ApplicationUserId" });
            DropColumn("dbo.Feeds", "ApplicationUserId");
        }
    }
}
