﻿using Microsoft.AspNet.Identity.EntityFramework;
using NewsFeedReader.Domain;
using System.Data.Entity;

namespace NewsFeedReader.DataAccess
{
  public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private static readonly object Lock = new object();
        private static bool databaseInitialized;

        public ApplicationDbContext() : this("DefaultConnection")
        {
        }

        public ApplicationDbContext(string connectionString)
            : base(connectionString, throwIfV1Schema: false)
        {
            //Database.Log = logger.Log;
            if (databaseInitialized)
            {
                return;
            }

            lock (Lock)
            {
                if (!databaseInitialized)
                {
                    // Set the database intializer which is run once during application start
                    // This seeds the database with admin user credentials and admin role
                    Database.SetInitializer(new CreateDatabaseIfNotExists<ApplicationDbContext>());
                    databaseInitialized = true;
                }
            }
        }

        public virtual IDbSet<Feed> Feeds { get; set; }
    }
}