﻿namespace NewsFeedReader.Domain.Repositories
{
    public interface IFeedRepository : IRepository<Feed>
    {
    }
}