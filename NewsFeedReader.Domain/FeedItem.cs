﻿using System;

namespace NewsFeedReader.Domain
{
    public class FeedItem
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDate { get; set; }  
        public Feed Feed { get;set; }
    }
}