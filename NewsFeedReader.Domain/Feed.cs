﻿using System.ComponentModel.DataAnnotations;

namespace NewsFeedReader.Domain
{
    public class Feed : BaseEntity
    {
        [StringLength(1000)]
        [Required]
        public string Url { get; set; }

        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        [Required]
        public string ApplicationUserId { get; set; }
    }
}