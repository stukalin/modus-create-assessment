﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using NewsFeedReader.BusinessLogic;
using NewsFeedReader.Domain;
using NewsFeedReader.Web.Models.Feed;

namespace NewsFeedReader.Web.Controllers
{
    [Authorize]
    public class FeedController : Controller
    {
        private readonly IFeedService feedService;

        public FeedController(IFeedService feedService)
        {
            this.feedService = feedService;
        }

        public ActionResult Index()
        {
            var allFeeds = Mapper.Map<List<FeedModel>>(feedService.GetAllFeeds(User.Identity.GetUserId()));
            return View(allFeeds);
        }

        public ActionResult View(int feedId)
        {
            try
            {
                return View(Mapper.Map<List<FeedItemModel>>(feedService.FetchFeedItems(User.Identity.GetUserId(), feedId)));
            }
            catch (FeedNotFoundException ex)
            {
                throw new HttpException(404, "Not found", ex);
            }
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(FeedModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var feed = Mapper.Map<Feed>(model);
            feed.ApplicationUserId = User.Identity.GetUserId();
            feedService.AddFeed(feed);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int feedId)
        {
            try
            {
                var feed = feedService.GetFeed(User.Identity.GetUserId(), feedId);
                return View(Mapper.Map<FeedModel>(feed));
            }
            catch (FeedNotFoundException ex)
            {
                throw new HttpException(404, "Not found", ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(FeedModel feedModel)
        {
            try
            {
                var feed = feedService.GetFeed(User.Identity.GetUserId(), feedModel.Id);
                feedService.DeleteFeed(feed);
                return RedirectToAction("Index");
            }
            catch (FeedNotFoundException ex)
            {
                throw new HttpException(404, "Not found", ex);
            }
        }

        public ActionResult Search(string search)
        {
            var feedItems = feedService.FetchFeedItems(User.Identity.GetUserId(), search);
            return View(Mapper.Map<List<FeedItemModel>>(feedItems));
        }
    }
}