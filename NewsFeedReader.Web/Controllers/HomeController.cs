﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using NewsFeedReader.BusinessLogic;
using NewsFeedReader.Web.Models;
using NewsFeedReader.Web.Models.Feed;

namespace NewsFeedReader.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFeedService feedService;

        public HomeController(IFeedService feedService)
        {
            this.feedService = feedService;
        }

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                var feedItems = feedService.FetchFeedItems(User.Identity.GetUserId());
                return View(Mapper.Map<List<FeedItemModel>>(feedItems));
            }

            return View();
        }

        public ActionResult Error(Exception exception)
        {
            int? code = null;
            if (exception is HttpException httpException)
            {
                code = httpException.GetHttpCode();
            }
            
            var message = $"An error came with the code {code}";

            if (code == 404)
            {
                message = "Oops! Not found";
            }

            if (code == null)
            {
                message = $"Unknown error with the message {exception.Message}";
            }

            return View("Error", new ErrorModel() {Message = message});
        }
    }
}