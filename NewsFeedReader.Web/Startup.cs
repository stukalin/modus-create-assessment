﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewsFeedReader.Web.Startup))]
namespace NewsFeedReader.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
