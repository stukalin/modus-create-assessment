﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsFeedReader.Web.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }
    }
}