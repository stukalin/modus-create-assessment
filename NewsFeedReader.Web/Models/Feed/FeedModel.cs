﻿using System.ComponentModel.DataAnnotations;

namespace NewsFeedReader.Web.Models.Feed
{
    public class FeedModel
    {
        public int Id { get; set; }

        [Required]
        [Url]
        [StringLength(1000)]
        public string Url { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}