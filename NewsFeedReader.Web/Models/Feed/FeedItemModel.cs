﻿using System;

namespace NewsFeedReader.Web.Models.Feed
{
    public class FeedItemModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDate { get; set; }  
        public FeedModel Feed { get;set; }
    }
}