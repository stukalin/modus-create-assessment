﻿using System.Data.Entity;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using NewsFeedReader.BusinessLogic;
using NewsFeedReader.DataAccess;

namespace NewsFeedReader.Web
{
    public class IocConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule<AutofacWebTypesModule>();

            builder.RegisterAssemblyTypes(typeof(ApplicationDbContext).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<FeedService>().As<IFeedService>().InstancePerRequest();

            builder.Register<DbContext>(c => new ApplicationDbContext("DefaultConnection"));
            builder.RegisterModule(new IdentityIocModule());

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}