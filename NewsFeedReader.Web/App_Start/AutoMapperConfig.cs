﻿using AutoMapper;
using NewsFeedReader.Domain;
using NewsFeedReader.Web.Models.Feed;

namespace NewsFeedReader.Web
{
    public class AutoMapperConfig
    {
        public static void Config()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Feed, FeedModel>();

                config.CreateMap<FeedModel, Feed>()
                    .ForMember(d => d.ApplicationUserId, o => o.Ignore())
                    .ForMember(d => d.ApplicationUser, o => o.Ignore());

                config.CreateMap<FeedItem, FeedItemModel>();
            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}