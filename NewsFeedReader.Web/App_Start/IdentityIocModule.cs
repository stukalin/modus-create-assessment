﻿using System.Data.Entity;
using System.Web;
using Autofac;
using Microsoft.AspNet.Identity.Owin;
using NewsFeedReader.Domain;

namespace NewsFeedReader.Web
{
    public class IdentityIocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(b =>
            {
                var manager = ApplicationUserManager.Create(b.Resolve<DbContext>());
                if (Startup.DataProtectionProvider != null)
                {
                    manager.UserTokenProvider =
                        new DataProtectorTokenProvider<ApplicationUser>(
                            Startup.DataProtectionProvider.Create("ASP.NET Identity"));
                }

                return manager;
            }).InstancePerRequest();

            builder.RegisterType<ApplicationSignInManager>().As<ApplicationSignInManager>().InstancePerRequest();
            builder.Register(b => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
        }
    }
}