Technical Interview: Asp.net / C#
=============================

**Purpose**

The purpose of this exercise is to measure the candidate�s ability to
satisfy business rules, while also demonstrating their ability to
further enhance the solution. The solution should not only satisfy the
business requirements outlined below, but should also give the
candidate an opportunity to show-off their skills.

We should be able to pull down the source, hit F5 in Visual Studio and
the solution should run with all databases created and populated.

We will reject submissions that do not follow this rule.

Once you think you have finished, it is suggested that you wipe all
remnants of your application from your computer (delete databases,
etc), clone the repository into a different directory and try to run
it to ensure it works properly.

Assume we have Visual Studio 2015/2017 and SQL Local DB on our boxes.
If your app needs something outside of those requirements, make sure
you bootstrap that in your application code so we don't have to
manually install any dependencies when looking at your solution.

Nuget packages you wish to add are perfectly acceptable.

**Pre-Interview Requirements**

1. User should be able to subscribe to a news feed.
2. User should be able to view items in a news feed.
3. User should be able to search for news feed items.
4. User should be able to see a listing of all news items from all
feeds.
5. Add any features / functionality that you would want in a feed
reader.

**Tip**

It is NOT expected that this will be an enterprise ready web
application.