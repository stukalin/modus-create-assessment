﻿using System.Collections.Generic;
using NewsFeedReader.Domain;

namespace NewsFeedReader.BusinessLogic
{
    public interface IFeedService
    {
        List<Feed> GetAllFeeds(string userId);
        void AddFeed(Feed feed);
        List<FeedItem> FetchFeedItems(string userId, int feedId);
        Feed GetFeed(string userId, int feedId);
        void DeleteFeed(Feed feed);
        List<FeedItem> FetchFeedItems(string userId, string searchTerm = null);
    }
}