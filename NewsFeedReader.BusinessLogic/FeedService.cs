﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using NewsFeedReader.Domain;
using NewsFeedReader.Domain.Repositories;

namespace NewsFeedReader.BusinessLogic
{
    public class FeedService : IFeedService
    {
        private readonly IFeedRepository feedRepository;

        public FeedService(IFeedRepository feedRepository)
        {
            this.feedRepository = feedRepository;
        }

        public List<Feed> GetAllFeeds(string userId)
        {
            return feedRepository.FindBy(f => f.ApplicationUserId == userId);
        }

        public void AddFeed(Feed feed)
        {
            feedRepository.Insert(feed);
        }

        public List<FeedItem> FetchFeedItems(string userId, string searchTerm = null)
        {
            var feeds = GetAllFeeds(userId);
            return feeds.SelectMany(feed => FetchFeedItems(feed, searchTerm)).ToList();
        }

        public List<FeedItem> FetchFeedItems(string userId, int feedId)
        {
            var feed = GetFeed(userId, feedId);
            return FetchFeedItems(feed);
        }

        private List<FeedItem> FetchFeedItems(Feed feed, string searchTerm = null)
        {
            XmlReader reader = XmlReader.Create(feed.Url);
            SyndicationFeed syndicationFeed = SyndicationFeed.Load(reader);
            reader.Close();

            var syndicationFeedItems = syndicationFeed.Items;
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                syndicationFeedItems = syndicationFeedItems.Where(a =>
                    a.Title.Text.IndexOf(searchTerm, StringComparison.InvariantCultureIgnoreCase) >= 0 
                    || a.Summary.Text.IndexOf(searchTerm, StringComparison.InvariantCultureIgnoreCase) >= 0);
            }

            return syndicationFeedItems.Select(sf => new FeedItem()
            {
                Url = sf.Links.FirstOrDefault()?.Uri.ToString(),
                Name = sf.Title.Text,
                Text = sf.Summary.Text,
                PublicationDate = sf.PublishDate.LocalDateTime,
                Feed = feed
            }).OrderByDescending(a => a.PublicationDate).ToList();
        }

        public Feed GetFeed(string userId, int feedId)
        {
            var feed = feedRepository.GetSingle(feedId);
            if (feed == null || feed.ApplicationUserId != userId)
            {
                throw new FeedNotFoundException();
            }

            return feed;
        }

        public void DeleteFeed(Feed feed)
        {
            feedRepository.Delete(feed);
        }
    }
}